﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            string source = "123a";
            int num1 = 5;
            int num2 = 20;            

            Task1 task1 = new Task1();
            int resultParseAndValidateIntegerNumber = task1.ParseAndValidateIntegerNumber(source);
            int resultMuptiplication = task1.Multiplication(num1, num2);
            Console.WriteLine(resultParseAndValidateIntegerNumber);
            Console.WriteLine(resultMuptiplication);

            string input = "144";
            int result = 0;
            int naturalNumber = 6;

            Task2 task2 = new Task2();
            bool resultTryParseTask2 = task2.TryParseNaturalNumber(input, out result);
            List<int> resultGetEvenNumbers = task2.GetEvenNumbers(naturalNumber);
            Console.WriteLine(resultTryParseTask2);
            foreach (int i in resultGetEvenNumbers)
            {
                Console.WriteLine(i);
            }

            int sourceTask3 = 5683;
            int digitToRemove = 8;

            Task3 task3 = new Task3();
            bool resultTryParseTask3 = task3.TryParseNaturalNumber(input, out result);
            string resultRemoveDigitFromNumber = task3.RemoveDigitFromNumber(sourceTask3, digitToRemove);
            Console.WriteLine(resultTryParseTask3);
            Console.WriteLine(resultRemoveDigitFromNumber);
        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            if (!Int32.TryParse(source, out int number))
            {
                throw new System.ArgumentException("Error. Invalid number.");
            }
            return Int32.Parse(source);
        }

        public int Multiplication(int num1, int num2)
        {
            int result = 0;

            if (num2 > 0)
            {
                for (int i = 1; i <= num2; i++)
                {
                    result += num1;
                }
            }

            if (num2 < 0)
            {
                for (int i = 1; i <= -num2; i++)
                {
                    result += -num1;
                }
            }

            return result;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (Int32.TryParse(input, out result))
            {
                return true;
            }
            return false;
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> evenNumbers = new List<int>();
            if (naturalNumber % 2 == 0)
            {
                for (int i = 0; i < naturalNumber * 2; i += 2)
                {
                    evenNumbers.Add(i);
                }
            }
            else
            {
                GetEvenNumbers(naturalNumber);
            }
            return evenNumbers;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (Int32.TryParse(input, out result))
            {
                return true;
            }
            return false;
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            string inputValue = Convert.ToString(source);
            string numberToRemove = Convert.ToString(digitToRemove);
            inputValue = inputValue.Replace(numberToRemove, string.Empty);
            return inputValue;
        }
    }
}
